# How To Convert Excel Data Into List Of Java Objects using Poiji API

### Things todo list:

1. Clone this repository: `git clone https://gitlab.com/hendisantika/spring-boot-pooiji.git`
2. Navigate to the folder: `cd spring-boot-pooiji`
3. Run the application: `mvn clean spring-boot:run`

### Image Screenshots

Upload File

![Upload File](img/upload.png "Upload File")

Success Upload File

![Success Upload File](img/success.png "Success Upload File")

Total Success Upload File

![Total Success Upload File](img/total.png "Total Success Upload File")

Poiiji Success Upload File

![Poiiji Success Upload File](img/poiiji.png "Poiiji Success Upload File")

DB

![DB](img/db.png "DB")
