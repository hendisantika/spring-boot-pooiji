package com.hendisantika;

import com.hendisantika.model.InvoiceExcel;
import com.poiji.bind.Poiji;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-pooiji
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/24/22
 * Time: 10:27
 * To change this template use File | Settings | File Templates.
 */
public class ExcelDataToJavaListTest {
    @Test
    void generateExcel() {
        File file = new File("/Users/powercommerce/Desktop/InvoiceDetailsSheet.xlsx");
        List<InvoiceExcel> invoices = Poiji.fromExcel(file, InvoiceExcel.class);
        System.out.println("Printing List Data: " + invoices);
    }
}
