package com.hendisantika.model;

import com.poiji.annotation.ExcelCell;
import com.poiji.annotation.ExcelCellName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-pooiji
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/24/22
 * Time: 10:29
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Invoice {

    @Id
    @GeneratedValue
    private Integer id;

    @ExcelCellName("Name") // ("Name") is the column name in excel
    private String name;

    @ExcelCell(1)    // (1) indicates excel column # 1
    private Double amount;

    @ExcelCell(2)   // (2) indicates excel column # 2
    private String number;

    @ExcelCellName("ReceivedDate") // ("ReceivedDate") is the column name in excel
    private String receivedDate;
}
