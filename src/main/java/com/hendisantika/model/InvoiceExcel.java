package com.hendisantika.model;

import com.poiji.annotation.ExcelCellName;
import com.poiji.annotation.ExcelRow;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-pooiji
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/24/22
 * Time: 10:25
 * To change this template use File | Settings | File Templates.
 */
public class InvoiceExcel {
    @ExcelRow
    private int rowIndex;

    @ExcelCellName("Name")
    private String name;

    @ExcelCellName("Amount")
    private Double amount;

    @ExcelCellName("Number")
    private String number;

    @ExcelCellName("ReceivedDate")
    private String receivedDate;

    @Override
    public String toString() {
        return "InvoiceExcel [rowIndex=" + rowIndex + ", name=" + name + ", amount=" + amount + ", number=" + number
                + ", receivedDate=" + receivedDate + "]";
    }
}
