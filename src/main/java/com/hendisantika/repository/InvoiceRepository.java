package com.hendisantika.repository;

import com.hendisantika.model.Invoice;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-pooiji
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/24/22
 * Time: 10:31
 * To change this template use File | Settings | File Templates.
 */
public interface InvoiceRepository extends JpaRepository<Invoice, Integer> {
}
