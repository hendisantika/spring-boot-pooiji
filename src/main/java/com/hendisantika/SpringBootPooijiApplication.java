package com.hendisantika;

import com.hendisantika.repository.InvoiceRepository;
import com.hendisantika.service.ExcelPoijiService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class SpringBootPooijiApplication implements CommandLineRunner {

    private final InvoiceRepository invoiceRepository;

    private final ExcelPoijiService excelPoijiService;

    public static void main(String[] args) {
        SpringApplication.run(SpringBootPooijiApplication.class, args);
    }

    @Override
    public void run(String... args) {
        invoiceRepository.deleteAll();
        invoiceRepository.saveAll(excelPoijiService.getListfromExcelData());
    }
}
