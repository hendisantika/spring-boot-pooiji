package com.hendisantika.service;

import com.hendisantika.model.Invoice;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-pooiji
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/24/22
 * Time: 11:38
 * To change this template use File | Settings | File Templates.
 */
@Service
@Slf4j
public class FileUploaderService {
    @Value("${app.upload.dir:${user.home}}")
    public String uploadDir;

    public List<Invoice> invoiceExcelReaderService() {
        return null;
    }

    public void uploadFile(MultipartFile file) {
        log.info("Uploading data ...");
        try {
            Path copyLocation = Paths
                    .get(uploadDir + File.separator + StringUtils.cleanPath(file.getOriginalFilename()));
            Files.copy(file.getInputStream(), copyLocation, StandardCopyOption.REPLACE_EXISTING);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Could not store file " + file.getOriginalFilename()
                    + ". Please try again!");
        }
    }
}
