package com.hendisantika.service;

import com.hendisantika.model.Invoice;
import com.poiji.bind.Poiji;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-pooiji
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/24/22
 * Time: 10:32
 * To change this template use File | Settings | File Templates.
 */
@Service
@Slf4j
public class ExcelPoijiService {
    @Value("${filePath}")
    public String FILE_PATH;

    public List<Invoice> getListfromExcelData() {
        log.info("FILE_PATH: " + FILE_PATH);
        File file = new File(FILE_PATH);
        return Poiji.fromExcel(file, Invoice.class);
    }
}
